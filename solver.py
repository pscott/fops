import fops
import numpy as np

class SolverNewton(object):
	def __init__(self, fun):
		self.fun = fun
		assert(len(fun.vars) == len(fun.outs))
		self.x = np.array([x.init for x in fun.vars])
		self.dx = np.zeros(len(fun.vars))
		self.nval = np.zeros(len(fun.outs))
		self.jac = np.zeros((len(fun.outs), len(fun.vars)))

	def iterate(self):
		self.fun.set_nval(self.x, self.nval)
		self.fun.set_jac(self.x, self.jac)
		#print(self.nval)
		#print(self.jac)
		try:
			self.dx = np.linalg.solve(self.jac, self.nval)
		except np.linalg.LinAlgError:
			print('Warning: singular matrix, ', end='')
			ret = np.linalg.lstsq(self.jac, self.nval)
			print('rank ' + str(ret[2]) + ' of ' +  str(len(self.x)))
			self.dx = ret[0]
		self.x += self.dx

	def solve(self, maxiter=100, tol=1e-6, verbose=False):
		for k in range(maxiter):
			self.iterate()
			res = max(abs(dx) for dx in self.dx)
			if verbose:
				print(str(k+1) + ' ' + str(res))
				#print(self.x)
			if res < tol:
				return k + 1
		return -1

if __name__ == '__main__':
	f = fops.Function()

	x = f.add_var()
	y = f.add_var()

	f.add_out(x*y + 2*y + 3)
	f.add_out(x + 5)

	solver = SolverNewton(f)
	it = solver.solve(maxiter=10)
	print(it)
