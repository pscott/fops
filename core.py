import math

def convert(x):
	if isinstance(x, (float, int)):
		return Const(x)
	return x

def conjugate(x):
	if isinstance(x, Complex):
		return Complex(x.re, -x.im)
	return x

class Complex(object):
	def __init__(self, re, im):
		self.re = re
		self.im = im

	def __add__(self, other):
		if isinstance(other, Complex):
			return Complex(self.re + other.re, self.im + other.im)
		return Complex(self.re + other, self.im)

	def __radd__(self, other):
		if isinstance(other, Complex):
			return Complex(other.re + self.re, other.im + self.im)
		return Complex(other + self.re, self.im)

	def __sub__(self, other):
		if isinstance(other, Complex):
			return Complex(self.re - other.re, self.im - other.im)
		return Complex(self.re - other, self.im)

	def __rsub__(self, other):
		if isinstance(other, Complex):
			return Complex(other.re - self.re, other.im - self.im)
		return Complex(other - self.re, self.im)

	def __neg__(self):
		return Complex(-self.re, -self.im)

	def __mul__(self, other):
		if isinstance(other, Complex):
			return Complex(self.re*other.re - self.im*other.im,
					self.re*other.im + self.im*other.re)
		return Complex(self.re*other, self.im*other)

	def __rmul__(self, other):
		if isinstance(other, Complex):
			return Complex(other.re*self.re - other.im*self.im,
					other.re*self.im + other.im*self.re)
		return Complex(other*self.re, other*self.im)

	def __str__(self):
		return '(' + str(self.re) + ', ' + str(self.im) + 'i)'

	def polar(self):
		return ((self.re**2 + self.im**2)**0.5,
				math.atan2(self.im, self.re)*180/math.pi)

class Expr(object):
	def __add__(self, other):
		return Add(self, convert(other))

	def __radd__(self, other):
		return Add(convert(other), self)

	def __sub__(self, other):
		return Sub(self, convert(other))

	def __rsub__(self, other):
		return Sub(convert(other), self)

	def __neg__(self):
		return Neg(self)

	def __mul__(self, other):
		return Mult(self, convert(other))

	def __rmul__(self, other):
		return Mult(convert(other), self)

class Var(Expr):
	def __init__(self, ind, init=0.0):
		self.ind = ind
		self.init = init

	def val(self, x):
		return x[self.ind]

	def dval(self, x, dind):
		if dind == self.ind:
			return 1.0
		return 0.0

	def includes(self, vs):
		vs.add(self.ind)

	def __str__(self):
		return 'x' + str(self.ind)

class Const(Expr):
	def __init__(self, v):
		self.v = v

	def val(self, x):
		return self.v

	def dval(self, x, dind):
		return 0.0

	def includes(self, vs):
		pass

	def __str__(self):
		return str(self.v)

class Add(Expr):
	def __init__(self, e1, e2):
		self.e1 = e1
		self.e2 = e2

	def val(self, x):
		return self.e1.val(x) + self.e2.val(x)

	def dval(self, x, dind):
		return self.e1.dval(x, dind) + self.e2.dval(x, dind)

	def includes(self, vs):
		self.e1.includes(vs)
		self.e2.includes(vs)

	def __str__(self):
		return '(' + str(self.e1) + ' + ' + str(self.e2) + ')'

class Sub(Expr):
	def __init__(self, e1, e2):
		self.e1 = e1
		self.e2 = e2

	def val(self, x):
		return self.e1.val(x) - self.e2.val(x)

	def dval(self, x, dind):
		return self.e1.dval(x, dind) - self.e2.dval(x, dind)

	def includes(self, vs):
		self.e1.includes(vs)
		self.e2.includes(vs)

	def __str__(self):
		return '(' + str(self.e1) + ' - ' + str(self.e2) + ')'

class Neg(Expr):
	def __init__(self, e1):
		self.e1 = e1

	def val(self, x):
		return -self.e1.val(x)

	def dval(self, x, dind):
		return -self.e1.dval(x, dind)

	def includes(self, vs):
		self.e1.includes(vs)

	def __str__(self):
		return '-' + str(self.e1)

class Mult(Expr):
	def __init__(self, e1, e2):
		self.e1 = e1
		self.e2 = e2

	def val(self, x):
		return self.e1.val(x)*self.e2.val(x)

	def dval(self, x, dind):
		return (self.e1.val(x)*self.e2.dval(x, dind)
				+ self.e1.dval(x, dind)*self.e2.val(x))

	def includes(self, vs):
		self.e1.includes(vs)
		self.e2.includes(vs)

	def __str__(self):
		return str(self.e1) + '*' + str(self.e2)

class Function(object):
	def __init__(self):
		self.vars = []
		self.outs = []

	def add_var(self, **kwargs):
		self.vars.append(Var(len(self.vars), **kwargs))
		return self.vars[-1]

	def add_out(self, expr):
		inds = set()
		expr.includes(inds)
		self.outs.append((expr, inds))
		return expr

	def add_cout(self, cexpr):
		self.add_out(cexpr.re)
		self.add_out(cexpr.im)
		return cexpr

	def set_val(self, x, v):
		for i, (e, inds) in enumerate(self.outs):
			v[i] = e.val(x)

	def set_nval(self, x, v):
		for i, (e, inds) in enumerate(self.outs):
			v[i] = -e.val(x)

	def set_jac(self, x, j):
		for i, (e, inds) in enumerate(self.outs):
			for ind in inds:
				j[i][ind] = e.dval(x, ind)

if __name__ == '__main__':
	f = Function()

	x = f.add_var()
	y = f.add_var()

	print(x + y)
	print(3 + y)
	print(3.0 + y)
	print(x + 3)
	print(x + 3.0)

	print((x + 3.0).val([1, 2]))
	print((x + 3.0).dval([1, 2], 0))
	print((x + 3.0).dval([1, 2], 1))

	f.add_out(x*y + 2*y + 3)
	f.add_out(x + 5)

	val = [0.]*len(f.outs)
	jac = [[0.]*len(f.vars)]*len(f.outs)

	xv = [0, 1]

	f.set_val(xv, val)
	f.set_jac(xv, jac)

	print(val)
	print(jac)

	c1 = Complex(1, 4)
	c2 = Complex(-5, 3)
	print(c1)
	print(c2)
	print(c1 + c2)
	print(c1*c2)
	print(-c1)
	print(conjugate(c2))
